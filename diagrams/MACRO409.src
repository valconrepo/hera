***********************************************************************
*
*      I.P.P. DELEK SOREK
* COMBINED CYCLE POWER PLANT
*
***********************************************************************
*
* Description : MACRO409 - PUMP with control
*
* MODIFICATION HISTORY (latest first):
*
* DATE  : 19-FEB-2016
* VERSION: 1
* AUTHOR: VALCON G. Mazeika/ Z.Bieris
* CHANGES: ellipse for background line updated, it will be executed one time in foreground;
* source aligned;
*
* DATE  : Unknown
* VERSION : 0
* AUTHOR : EMERSON/VALCON
* CHANGES  : Original / as found.
*
***********************************************************************

DIAGRAM MAIN 0 0 0 1916 1032 mwgrey0 ZOOMABLE 1 0 0 16384 16384 1 DEFAULT_POSITION
   DEFAULT_SIZE "PUMP with control"

DEF_MACRO_PARAMS 2 "FP_ALG" "EMERGENCY PB"
   0
   1 "TAG NAME"
   0
   2 "TAG POS: 1-bot,2-right,3-top,4-left" ["1"] "Orientation: 1-bot,2-right,3-top,4-left"
   ["1"]
   0
   0
   0



***   local variables   ***

DEF_VARIABLE LOCAL EDEV SYSID
DEF_VARIABLE LOCAL PSTA SYSID
DEF_VARIABLE LOCAL STAT SYSID
DEF_VARIABLE LOCAL DERR SYSID
DEF_VARIABLE LOCAL KBRD SYSID
DEF_VARIABLE LOCAL FG-ONE-TIME INT    *Rev.1 Draw object in foreground one time
BACKGROUND
SET_VARIABLE L:FG-ONE-TIME 1
SET_VARIABLE L:EDEV $D1 G0
SET_VARIABLE L:KBRD $D1 G1
SET_VARIABLE L:PSTA L:EDEV C2
SET_VARIABLE L:STAT L:EDEV C0
SET_VARIABLE L:DERR L:EDEV C1
RUN_PROGRAMS 1 227 0 1 $D1 G0

*draw only in graphic builder cause in runtime contst1 is in [1,4]
IF ($CONST1 = 5)
COLOR FG black BG white ER gray65 OL 0 BLINK FG OFF BG OFF
LINE 5721 5867 6440 5867 1 solid
ENDIF

*draw only in graphic builder cause in runtime contst1 is in [1,4]
IF($CONST1 = 5 )
LINE 6098 5071 6098 6461 1 solid
ENDIF

FONT "DotumChe" BOLD
COLOR FG black BG mwgrey0 ER mwgrey0 OL 0 BLINK FG OFF BG OFF

*** TAG POSITION
IF($CONST1 = 1) *TAG AT BOTTOM
TEXT 5838 6500  "$T1" HORZ VECTOR_OVER 70 265 1
ENDIF
IF($CONST1 = 2)  *TAG AT RIGHT
TEXT 6430 5986  "$T1" HORZ VECTOR_OVER 70 265 1
ENDIF
IF($CONST1 = 3)  *TAG AT TOP
TEXT 5630 4959  "$T1" HORZ VECTOR_OVER 70 265 1
ENDIF
IF($CONST1 = 4)  *TAG AT LEFT
TEXT 5082 5523  "$T1" HORZ VECTOR_OVER 70 265 1
ENDIF

FOREGROUND
IF (L:FG-ONE-TIME=1) *Rev.1 Draw object in foreground one time
COLOR FG mwgrey4 BG mwgrey0 ER mwgrey0 OL 0 BLINK FG OFF BG
   OFF
ELLIPSE 6098 5867 154 294 1 solid solid_bg
SET_VARIABLE L:FG-ONE-TIME 2
ENDIF

********mode
COLOR FG black { (L:PSTA A2 = ON3) black  (L:PSTA A2 = ON4) black  (L:PSTA A2 = ON5) black
   (((L:PSTA A2 = OFF3) AND (L:PSTA A2 = OFF4)) AND (L:PSTA A2 = OFF5)) black  } BG mwgrey0 {
   (L:PSTA A2 = ON3) mwgrey0  (L:PSTA A2 = ON4) yellow  (L:PSTA A2 = ON5) yellow  (((L:PSTA
   A2 = OFF3) AND (L:PSTA A2 = OFF4)) AND (L:PSTA A2 = OFF5)) cyan  } ER gray70 OL 0 BLINK FG
   OFF BG OFF
FONT "DotumChe" REGULAR
TEXT 6234 5295  "?" { (L:PSTA A2 = ON3) "A" (L:PSTA A2 = ON4) "M" (L:PSTA A2 = ON5) "L"
   (((L:PSTA A2 = OFF3) AND (L:PSTA A2 = OFF4)) AND (L:PSTA A2 = OFF5)) "T" } HORZ VECTOR_OVER
   96 239 1

COLOR FG mwgrey0 {
((((L:PSTA 2W = ON24) OR (L:PSTA 2W = ON25)) OR (L:PSTA 2W = ON26)) OR (L:PSTA A2 = ON7)) mwred1
((L:PSTA A2 = ON8) OR (L:PSTA A2 = ON12))  mwyellow1
(((L:PSTA 2W = OFF21) OR (L:PSTA 2W = OFF22) OR (L:PSTA 2W = OFF23))) mwcyan1
(L:PSTA 2W = OFF27) mwgrey4
(S:CONTROL_SID1= L:KBRD ID) mwgrey4  } BG mwgrey0 ER gray65 OL 0 BLINK FG OFF
   (S:CONTROL_SID1= L:KBRD ID) ON  BG OFF
LINE 5814 5748 5814 5269 6042 5269 2 solid (L:PSTA 2W = OFF27) dashed
LINE 6349 5745 6349 5269 6156 5269 2 solid (L:PSTA 2W = OFF27) dashed
LINE 5814 5987 5814 6261 6042 6261 2 solid (L:PSTA 2W = OFF27) dashed
LINE 6349 5987 6349 6261 6156 6261 2 solid (L:PSTA 2W = OFF27) dashed
*RECTANGLE 5833 5140 672 1241 2 solid (L:PSTA 2W = OFF27) dashed  unfilled


******************DEVICE SYMBOL**************************

IF ($CONST2=1) *************************************** BOTTOM ORIENTATION
COLOR FG black BG white {
(L:PSTA 1W = HDWRFAIL) magenta
((L:STAT AV >= 600) AND (L:STAT AV < 700)) white
((L:STAT AV >= 300) AND (L:STAT AV < 400)) white
(L:PSTA 2W = PSET16) white
(L:PSTA 2W = PSET17) mwgrey0
   } ER mwgrey0 OL 0 BLINK FG OFF BG OFF {
((L:STAT AV >= 600) AND (L:STAT AV < 700)) ON
((L:STAT AV >= 300) AND (L:STAT AV < 400)) ON
(L:PSTA 2W = PSET16) OFF
(L:PSTA 2W = PSET17) OFF  }
POLYGON 6099 6161 5969 5723 6230 5723 6099 6161 1 solid solid_bg
ENDIF

IF ($CONST2=2) *************************************** RIGHT ORIENTATION
COLOR FG black BG dodgerblue { (L:PSTA 1W = HDWRFAIL) magenta  ((L:STAT AV >= 600) AND
   (L:STAT AV < 700)) white  ((L:STAT AV >= 300) AND (L:STAT AV < 400)) white  (L:PSTA 2W =
   PSET16) white  (L:PSTA 2W = PSET17) mwgrey0  } ER mwgrey0 OL 0 BLINK FG OFF BG OFF {
   ((L:STAT AV >= 600) AND (L:STAT AV < 700)) ON  ((L:STAT AV >= 300) AND (L:STAT AV < 400))
   ON  (L:PSTA 2W = PSET16) OFF  (L:PSTA 2W = PSET17) OFF  }
POLYGON 6250 5869 6024 6124 6024 5614 6250 5869 1 solid solid_bg
ENDIF

IF ($CONST2=3) *************************************** TOP ORIENTATION
COLOR FG black BG white {
(L:PSTA 1W = HDWRFAIL) magenta
((L:STAT AV >= 600) AND (L:STAT AV < 700)) white
((L:STAT AV >= 300) AND (L:STAT AV < 400)) white
(L:PSTA 2W = PSET16) white
(L:PSTA 2W = PSET17) mwgrey0
   } ER mwgrey0 OL 0 BLINK FG OFF BG OFF {
(L:PSTA 2W = PSET16) OFF
(L:PSTA 2W = PSET17) OFF
((L:STAT AV >= 600) AND (L:STAT AV < 700)) ON
((L:STAT AV >= 300) AND (L:STAT AV < 400)) ON  }
POLYGON 6099 5575 6232 6011 5968 6011 6099 5575 1 solid solid_bg
ENDIF

IF ($CONST2=4) *************************************** LEFT ORIENTATION
COLOR FG black BG blue2 {
(L:PSTA 1W = HDWRFAIL) magenta
((L:STAT AV >= 600) AND (L:STAT AV < 700)) white
((L:STAT AV >= 300) AND (L:STAT AV < 400)) white
(L:PSTA 2W = PSET16) white
(L:PSTA 2W = PSET17) mwgrey0
   } ER mwgrey0 OL 0 BLINK FG OFF BG OFF {
(L:PSTA 2W = PSET16) OFF
(L:PSTA 2W = PSET17) OFF
((L:STAT AV >= 600) AND (L:STAT AV < 700)) ON
((L:STAT AV >= 300) AND (L:STAT AV < 400)) ON  }
POLYGON 5965 5869 6166 5634 6166 6104 5965 5869 1 solid solid_bg
ENDIF


*IF ($D2 <>0)
*IF ($D2 DS = RESET)
COLOR FG mwgrey0 (($D2 DS = RESET) AND ($D2 <>0)) red BG white ER mwgrey0 OL 0 BLINK FG OFF BG OFF
PIE 6233 6050 6233 5996 6256 5952 6282 5952 6309 5952 6332 5996 6332 6050 6332 6103 6309
   6146 6282 6146 6256 6146 6233 6103 6233 6050 6233 6048 6332 6048 6282 6049 1 solid solid
LINE 6283 6051 6283 6050 6283 6200 1 solid
RECTANGLE 6247 6197 75 28 1 solid solid
*ENDIF
*ENDIF

FORCE_UPDATE FALSE

KEYBOARD
COLOR FG black BG mwgrey0 ER gray65 OL 0 BLINK FG OFF BG OFF
POKE_FLD "POKE1" -1 5806 5257 551 1018 ON 7 2
   6 0 5 L:KBRD ID L:KBRD ID 1 99 2
   117 7405 6 0 200 200 0 1 $D1 ID
POKE_FLD "POKE1" -1 5806 5257 551 1018 ON 0 L:PSTA
